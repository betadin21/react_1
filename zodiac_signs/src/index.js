import React from "react";
import ReactDOM  from "react-dom";
import "./style.css";
function ZodiacSignsTable () {
    return (
        <table>
            <thead>
                <tr>
                    <th className="title" colspan="3">Zodiac Signs</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Козеріг</td>
                    <td>22.12-20.01</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Водолій</td>
                    <td>21.01-18.02</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Риби</td>
                    <td>19.02-20.03</td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Овен</td>
                    <td>21.03-20.04</td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>Тілець</td>
                    <td>21.04-21.05</td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>Близнюки</td>
                    <td>22.05-21.06</td>
                </tr>
                <tr>
                    <td>7</td>
                    <td>Рак</td>
                    <td>22.06-22.07</td>
                </tr>
                <tr>
                    <td>8</td>
                    <td>Лев</td>
                    <td>23.07-23.08</td>
                </tr>
                <tr>
                    <td>9</td>
                    <td>Діва</td>
                    <td>24.08-23.09</td>
                </tr>
                <tr>
                    <td>10</td>
                    <td>Терези</td>
                    <td>24.09-23.10</td>
                </tr>
                <tr>
                    <td>11</td>
                    <td>Скорпіон</td>
                    <td>24.10-22.11</td>
                </tr>
                <tr>
                    <td>12</td>
                    <td>Стрілець</td>
                    <td>23.11-21.12</td>
                </tr>

            </tbody>
        </table>
    )
}
ReactDOM.render(<ZodiacSignsTable></ZodiacSignsTable>, document.querySelector(".one"))