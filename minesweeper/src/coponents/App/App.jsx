import React, { Component } from "react";
import Board from "../Board/Board";
import BoardHead from "../BoardHead/boardHead";
import "./style.css";

class App extends Component {
  state = {
    status: "running",
    rows: 10,
    columns: 10,
    flags: 10,
    mines: 10,
    time: 0,
    openCells: 0,
  };
  render() {
    return (
      <>
        <h1>Minesweeper</h1>
        <div className="minesweeper">
          <BoardHead time={this.state.time} flags={this.state.flags} />
          <Board
            rows={this.state.rows}
            columns={this.state.columns}
            mines={this.state.mines}
            openCells={this.state.openCells}
          />
        </div>
      </>
    );
  }
}

export default App;
