import Cell from "../Cell/cell"

const Row= ({cells}) => {
  let cell = cells.map((data, index)=>{
    return(
      <Cell
        data={data}
        key = {index}
      />
    )
  })
  return (
    <div className="row">{cell}</div>
  )
}

export default Row