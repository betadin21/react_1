
import Row from "../Row/row";
const Board = ({ rows, columns, mines, openCells }) => {
    // створюю дошку
    function createBoard(){
      let board = [];
      for (let i = 0; i < rows; i++) {
        board.push([]);
        for (let j = 0; j < columns; j++) {
          board[i].push({
            x: j,
            y: i,
            count: 0,
            isOpen: false,
            hasMine: false,
            hasFlag: false,
          });
        }
      }
    //   створюю міни
      for (let i = 0; i < mines; i++) {
        let randomRow = Math.floor(Math.random() * rows);
        let randomColumn = Math.floor(Math.random() * columns);
        let cell = board[randomRow][randomColumn];
        
        if(cell.hasMine){
            i--;
        }else{
            cell.hasMine = true; 
        }
      }
      return board;
    };
    let boardArr = createBoard();
    const open = cell =>{
      let current = boardArr[cell.y][cell.x];
      if(current.hasMine && openCells===0){
        console.log("клітинка вже має міну. Перезапустіть гру");
        let newRows = createBoard()
        boardArr = newRows;
        open()
      }else{
        
      }
    }
    open()
    // будую рядки на сторінці
    let buildRow = boardArr.map((row, index)=>{
        return(
            <Row 
                cells={row}
                key ={index}
            />
        )
    })
  
  return <div className="board">{buildRow}</div>;
};

export default Board;

