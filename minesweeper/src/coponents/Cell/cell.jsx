const Cell = (data) => {
  let renderCell = () => {
    if (data.isOpen) {
      return <div className="cell open"></div>;
    } else {
      return <div className="cell"></div>;
    }
  };
  return renderCell()
};

export default Cell;
