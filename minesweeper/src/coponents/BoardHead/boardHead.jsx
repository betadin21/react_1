const BoardHead = ({time,flags}) => {
    let minutes = Math.floor(time / 60);
    let seconds = time - minutes * 60 || 0;
    let formatSrconds = seconds < 10 ? `0${seconds}` : seconds;
    let timer = `${minutes}:${formatSrconds}`
  return (
    <div className="board__head">
        <div className="flags">{flags}</div>
        <button className="reset">Reset</button>
        <div className="timer">{timer}</div>
       
    </div>
  )
}

export default BoardHead;